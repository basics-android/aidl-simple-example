package es.diusframi.aidlclient;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import es.diusframi.aidlserver.IComman;

public class MainActivity extends AppCompatActivity {

    private IComman comman;
    private ServiceConnection serviceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            comman = IComman.Stub.asInterface(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void connectDisconnect(View v){
        Intent i = new Intent("es.diusframi.aidlserver.service.AIDL");
        // For security reasons intent must be convert to explicit one.
        bindService(ConvertImplicitIntentToExplicitOne(i), serviceConn, BIND_AUTO_CREATE);
    }
    public void add(View v){
        try {
            EditText etVarX = findViewById(R.id.editTextVarX);
            EditText etVarY = findViewById(R.id.editTextVarY);
            int res = comman.calculate(Integer.parseInt(etVarX.getText().toString()),Integer.parseInt(etVarY.getText().toString()));
            TextView tvResultAddition = findViewById(R.id.textViewResultAddtion);
            tvResultAddition.setText(""+res);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
    public Intent ConvertImplicitIntentToExplicitOne(Intent i){
        PackageManager pm = getPackageManager();
        List<ResolveInfo> resolveInfoList = pm.queryIntentServices(i,0);
        if (resolveInfoList == null || resolveInfoList.size() != 1){
            return null;
        }
        ResolveInfo serviceInfo = resolveInfoList.get(0);
        ComponentName component = new ComponentName(serviceInfo.serviceInfo.packageName,serviceInfo.serviceInfo.name);
        Intent explicitOne = new Intent(i);
        explicitOne.setComponent(component);
        return explicitOne;
    }
}